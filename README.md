# Python newsletter

PyCon UK 2019 CFP is open! The conference is at Cardiff, UK; the dates are 2019-09-13 (a Friday) to 2019-09-17 (the following Tuesday).
CFP closes on 2019-06-07 and is here: https://2019.pyconuk.org/call-proposals/. No ticket sales yet.

EuroPython is in Basel (Switzerland, I assume) from 2019-07-08 to 2019-07-14 and the Call for Proposals is open and closes on 2019-05-12.

No idea when the Jetbrains Python Survey was announced, but it's here: https://www.jetbrains.com/research/python-developers-survey-2018/

2019-03-25: Python 3.7.3 is released, and also 3.4.10 and 3.5.7 https://www.python.org/downloads/

It is always worth mentioning that Python 2 support finishes this year.